#ifndef SERIAL_H
#define SERIAL_H

#include <stdint.h>

// Running at 20 MHz, 9600 Baud rate

#ifdef __cplusplus
extern "C" {
#endif

void initSerial();
void putchar( uint8_t data);

#ifdef __cplusplus
}
#endif

#endif // SERIAL_H
