#ifndef PRINT_H
#define PRINT_H

#ifdef __cplusplus
extern "C" {
#endif

void printf(char* format,...);

#ifdef __cplusplus
}
#endif

#endif // PRINT_H
