#include "serial.h"
#include <avr/io.h>

void initSerial()
{
    UBRR0L = 129;
    UCSR0B = (1 << TXEN0);
}

void putchar(uint8_t data)
{
    while ( !( UCSR0A & (1<<UDRE0)) );
    UDR0 = data;
}
